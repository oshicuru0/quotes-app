package app.or.motivationsquotes.category.singlecategory;

import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import app.or.motivationsquotes.R;

import static app.or.motivationsquotes.utils.Constants.getColorForCategory;

public class CategoryAdapter extends RecyclerView.Adapter<CategoryAdapter.CategoryViewHolder> {
    private List<OneTwo> categoryList = new ArrayList<>();
    private CategoryListener categoryListener;

    CategoryAdapter() {
        createList();
    }

    class CategoryViewHolder extends RecyclerView.ViewHolder {
        View itemView, category1, category2;
        TextView title, subtitle, title1, subtitle1;

        CategoryViewHolder(View view) {
            super(view);
            itemView = view;
            title = itemView.findViewById(R.id.title);
            subtitle = itemView.findViewById(R.id.name);
            title1 = itemView.findViewById(R.id.title1);
            subtitle1 = itemView.findViewById(R.id.name1);
            category1 = itemView.findViewById(R.id.category1);
            category2 = itemView.findViewById(R.id.category2);
        }
    }

    @NonNull
    @Override
    public CategoryViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.category_item, parent, false);
        return new CategoryViewHolder(itemView);
    }


    @Override
    public void onBindViewHolder(@NonNull CategoryViewHolder holder, final int position) {
        final OneTwo oneTwo = categoryList.get(position);

        Drawable drawable = ContextCompat.getDrawable(holder.title.getContext(), R.drawable.item_shape);
        drawable.setColorFilter(new PorterDuffColorFilter(getColorForCategory(holder.title.getContext(), oneTwo.one), PorterDuff.Mode.MULTIPLY));

        holder.title.setText(oneTwo.o);
        holder.title.setBackground(drawable);
        holder.subtitle.setText(oneTwo.one);

        holder.title1.setText(oneTwo.t);
        Drawable drawable1 = ContextCompat.getDrawable(holder.title1.getContext(), R.drawable.item_shape);
        drawable1.setColorFilter(new PorterDuffColorFilter(getColorForCategory(holder.title.getContext(), oneTwo.two), PorterDuff.Mode.MULTIPLY));

        holder.title1.setBackground(drawable1);
        holder.subtitle1.setText(oneTwo.two);

        holder.category1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                categoryListener.onCategoryClicked(oneTwo.one);
            }
        });

        holder.category2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                categoryListener.onCategoryClicked(oneTwo.two);
            }
        });
    }

    public void setCategoryListener(CategoryListener categoryListener) {
        this.categoryListener = categoryListener;
    }

    @Override
    public int getItemCount() {
        return categoryList.size();
    }

    public void setCategoryList(List<OneTwo> categoryList) {
        this.categoryList = categoryList;
        notifyDataSetChanged();
    }

    private void createList() {
        categoryList.add(new OneTwo("Quote of the day", "Bookmarked"));
        categoryList.add(new OneTwo("Work", "Motivational"));
        categoryList.add(new OneTwo("Politics", "Peace"));
        categoryList.add(new OneTwo("Opportunity", "Humanity"));
        categoryList.add(new OneTwo("Inspirational", "Love"));
        categoryList.add(new OneTwo("Attitude", "Leadership"));
        categoryList.add(new OneTwo("Truth", "Enthusiasm"));
    }

    private class OneTwo {
        public String one, two;
        public String o, t;

        public OneTwo(String one, String two) {
            this.one = one;
            this.two = two;
            this.o = one.substring(0, 1);
            this.t = two.substring(0, 1);
        }
    }

    public interface CategoryListener {
        void onCategoryClicked(String category);
    }
}