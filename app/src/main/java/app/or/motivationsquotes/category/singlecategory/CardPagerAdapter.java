package app.or.motivationsquotes.category.singlecategory;

import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import app.or.motivationsquotes.R;
import app.or.motivationsquotes.activity.MainActivity;
import app.or.motivationsquotes.helper.FirebaseHelper;
import app.or.motivationsquotes.utils.Tools;

public class CardPagerAdapter extends PagerAdapter implements CardAdapter {
    private final MainActivity mainActivity;
    private List<CardItem> cards = new ArrayList<>();
    private float mBaseElevation;
    private List<CardView> mViews = new ArrayList<>();

    public CardPagerAdapter(MainActivity mainActivity) {
        this.mainActivity = mainActivity;
    }

    public void setCards(List<CardItem> cards) {
        this.cards = cards;
        notifyDataSetChanged();
        for (int i = 0; i < cards.size(); i++) {
            mViews.add(null);
        }
    }

    public float getBaseElevation() {
        return mBaseElevation;
    }

    @Override
    public CardView getCardViewAt(int position) {
        return mViews.isEmpty() ? null : mViews.get(position);
    }

    @Override
    public int getCount() {
        return cards.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        View view = LayoutInflater.from(container.getContext()).inflate(R.layout.adapter, container, false);
        container.addView(view);
        bind(cards.get(position), view);
        CardView cardView = view.findViewById(R.id.cardView);

        if (mBaseElevation == 0) {
            mBaseElevation = cardView.getCardElevation();
        }

        cardView.setMaxCardElevation(mBaseElevation * MAX_ELEVATION_FACTOR);
        if (position < mViews.size()) {
            mViews.set(position, cardView);
        }
        return view;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((View) object);
        mViews.set(position, null);
    }

    private void bind(final CardItem item, View view) {
        final TextView like = view.findViewById(R.id.like);
        final ImageButton download = view.findViewById(R.id.download);
        final ImageButton bookmark = view.findViewById(R.id.bookmark);
        TextView author = view.findViewById(R.id.author);
        TextView quote = view.findViewById(R.id.quote);
        ImageView authorImage = view.findViewById(R.id.authorImage);

        author.setText(String.format("- %s", item.getAuthor()));
        quote.setText(item.getQuote());

        Picasso.get().load(item.getAuthorImage()).into(authorImage);

        like.setCompoundDrawablesRelativeWithIntrinsicBounds(item.getLikedBy().contains(Tools.getUniqueIdentification(mainActivity)) ? R.drawable.ic_heart_filled : R.drawable.ic_heart_border,
                0, 0, 0);
        like.setText(String.valueOf(item.getLikedBy().size()));
        like.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FirebaseHelper.like(item, mainActivity);
            }
        });

        download.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mainActivity.download(item);
            }
        });

        bookmark.setImageResource(item.getBookmarkBy().contains(Tools.getUniqueIdentification(mainActivity)) ? R.drawable.ic_bookmark_filled : R.drawable.ic_bookmark_border);

        bookmark.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FirebaseHelper.bookmark(item, mainActivity);
            }
        });
    }
}