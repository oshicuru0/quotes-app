package app.or.motivationsquotes.category.singlecategory;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class CardItem implements Serializable{
    private String id;
    private String author;
    private String authorImage;
    private String quote;
    private List<String> tags;
    private List<String> bookmarkBy;
    private List<String> likedBy;

    public CardItem() {}

    public CardItem(String id, String author, String authorImage, String quote, List<String> tags) {
        this.id = id;
        this.author = author;
        this.authorImage = authorImage;
        this.quote = quote;
        this.tags = tags;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getAuthorImage() {
        return authorImage;
    }

    public void setAuthorImage(String authorImage) {
        this.authorImage = authorImage;
    }

    public String getQuote() {
        return quote;
    }

    public void setQuote(String quote) {
        this.quote = quote;
    }

    public void setTags(List<String> tags) {
        this.tags = tags;
    }

    public List<String> getTags() {
        return tags;
    }

    public void setBookmarkBy(List<String> bookmarkBy) {
        this.bookmarkBy = bookmarkBy;
    }

    public List<String> getBookmarkBy() {
        return bookmarkBy == null ? new ArrayList<String>() : this.bookmarkBy;
    }

    public void setLikedBy(List<String> likedBy) {
        this.likedBy = likedBy;
    }

    public List<String> getLikedBy() {
        return likedBy == null ? new ArrayList<String>() : this.likedBy;
    }
}