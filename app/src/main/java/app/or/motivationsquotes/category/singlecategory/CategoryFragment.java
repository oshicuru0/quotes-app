package app.or.motivationsquotes.category.singlecategory;

import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

import app.or.motivationsquotes.activity.MainActivity;
import app.or.motivationsquotes.R;
import app.or.motivationsquotes.utils.MyDivider;

@EFragment(R.layout.fragment_category)
public class CategoryFragment extends Fragment implements CategoryAdapter.CategoryListener {

    @ViewById
    protected RecyclerView categoryRecycler;

    private CategoryAdapter categoryAdapter = new CategoryAdapter();

    @AfterViews
    public void onCreate() {
        MainActivity mainActivity = (MainActivity) getActivity();
        if (mainActivity == null || mainActivity.isFinishing() || !isAdded()) {
            return;
        }

        mainActivity.setTitle("Category");

        categoryRecycler.setLayoutManager(new LinearLayoutManager(mainActivity));
        categoryRecycler.addItemDecoration(new MyDivider(mainActivity, DividerItemDecoration.VERTICAL));
        categoryRecycler.setAdapter(categoryAdapter);
        categoryAdapter.setCategoryListener(this);
    }

    @Override
    public void onCategoryClicked(String category) {
        MainActivity mainActivity = (MainActivity) getActivity();
        if (mainActivity == null || mainActivity.isFinishing() || !isAdded()) {
            return;
        }

        mainActivity.openCategory(category);
    }
}
