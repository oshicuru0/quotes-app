package app.or.motivationsquotes.search;

import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.ProgressBar;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.FragmentArg;
import org.androidannotations.annotations.ViewById;

import java.util.List;

import app.or.motivationsquotes.R;
import app.or.motivationsquotes.activity.MainActivity;
import app.or.motivationsquotes.category.singlecategory.CardItem;
import app.or.motivationsquotes.category.singlecategory.CardPagerAdapter;
import app.or.motivationsquotes.category.singlecategory.ShadowTransformer;
import app.or.motivationsquotes.helper.FirebaseHelper;
import app.or.motivationsquotes.utils.Constants;

@EFragment(R.layout.fragment_category_items)
public class SearchFragment extends Fragment {

    @FragmentArg
    public String category;

    @ViewById
    public ViewPager viewPager;

    @ViewById
    public ProgressBar loader;

    private CardPagerAdapter mCardAdapter;

    int lastSelectedPosition = 0;

    @AfterViews
    public void onCreate() {
        MainActivity mainActivity = (MainActivity) getActivity();
        if (mainActivity == null || mainActivity.isFinishing() || !isAdded()) {
            return;
        }

        mCardAdapter = new CardPagerAdapter(mainActivity);
        viewPager.setPageTransformer(false, new ShadowTransformer(viewPager, mCardAdapter, true));
        viewPager.setOffscreenPageLimit(3);

        if (!Constants.getCategoryColors().containsKey(category)) {
            mainActivity.setTitle("Your quote");
            FirebaseHelper.getById(category, new FirebaseHelper.ResponseData() {
                @Override
                public void onResponse(List<CardItem> cardItems) {
                    mCardAdapter.setCards(cardItems);
                    viewPager.setAdapter(mCardAdapter);
                    loader.setVisibility(View.GONE);
                }
            });
            return;
        }

        mainActivity.setTitle(category);
        FirebaseHelper.getByCategory(mainActivity, category.toLowerCase(), new FirebaseHelper.ResponseData() {
            @Override
            public void onResponse(List<CardItem> cardItems) {
                if (viewPager == null) {
                    return;
                }

                lastSelectedPosition = viewPager.getCurrentItem();
                mCardAdapter.setCards(cardItems);
                viewPager.setAdapter(mCardAdapter);
                viewPager.setCurrentItem(lastSelectedPosition);
                loader.setVisibility(View.GONE);
            }
        });
    }
}
