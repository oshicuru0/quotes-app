package app.or.motivationsquotes.helper;

import android.content.Context;
import android.support.annotation.NonNull;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.firestore.SetOptions;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Nullable;

import app.or.motivationsquotes.activity.MainActivity;
import app.or.motivationsquotes.category.singlecategory.CardItem;
import app.or.motivationsquotes.utils.Tools;


/**
 * Created by RESIDOVI on 9/20/2018.
 */

public class FirebaseHelper {
    public static void getByCategory(MainActivity mainActivity, String category, final ResponseData responseData) {
        if (category.equals("bookmarked")) {
            getBookMarked(mainActivity, responseData);
            return;
        }

        FirebaseFirestore db = FirebaseFirestore.getInstance();
        db.collection("quotes")
                .whereArrayContains("tags", category)
                .addSnapshotListener(new EventListener<QuerySnapshot>() {
                    @Override
                    public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots, @Nullable FirebaseFirestoreException e) {
                        List<CardItem> cardItems = new ArrayList<>();
                        if (queryDocumentSnapshots != null && !queryDocumentSnapshots.isEmpty()) {
                            for (DocumentSnapshot document : queryDocumentSnapshots.getDocuments()) {
                                CardItem cardItem = document.toObject(CardItem.class);
                                cardItem.setId(document.getId());
                                cardItems.add(cardItem);
                            }
                        }

                        responseData.onResponse(cardItems);
                    }
                });
    }

    public static void searchByAuthor(String author, final ResponseData responseData) {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        db.collection("quotes")
                .addSnapshotListener((queryDocumentSnapshots, e) -> {
                    List<CardItem> cardItems = new ArrayList<>();
                    if (queryDocumentSnapshots != null && !queryDocumentSnapshots.isEmpty()) {
                        for (DocumentSnapshot document : queryDocumentSnapshots.getDocuments()) {
                            CardItem cardItem = document.toObject(CardItem.class);
                            cardItem.setId(document.getId());
                            if (cardItem.getAuthor().toLowerCase().contains(author.toLowerCase())) {
                                cardItems.add(cardItem);
                            }
                        }
                    }

                    responseData.onResponse(cardItems);
                });
    }

    public static void getById(String category, final ResponseData responseData) {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        db.collection("quotes")
                .document(category)
                .addSnapshotListener(new EventListener<DocumentSnapshot>() {
                    @Override
                    public void onEvent(@Nullable DocumentSnapshot documentSnapshot, @Nullable FirebaseFirestoreException e) {
                        List<CardItem> cardItems = new ArrayList<>();
                        if (documentSnapshot != null && documentSnapshot.exists()) {
                            CardItem cardItem = documentSnapshot.toObject(CardItem.class);
                            cardItem.setId(documentSnapshot.getId());
                            cardItems.add(cardItem);
                        }
                        responseData.onResponse(cardItems);
                    }
                });
    }

    public static void getBookMarked(Context context, final ResponseData responseData) {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        db.collection("quotes")
                .whereArrayContains("bookmarkBy", Tools.getUniqueIdentification(context))
                .addSnapshotListener(new EventListener<QuerySnapshot>() {
                    @Override
                    public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots, @Nullable FirebaseFirestoreException e) {
                        List<CardItem> cardItems = new ArrayList<>();
                        if (queryDocumentSnapshots != null && !queryDocumentSnapshots.isEmpty()) {
                            for (DocumentSnapshot document : queryDocumentSnapshots.getDocuments()) {
                                CardItem cardItem = document.toObject(CardItem.class);
                                cardItem.setId(document.getId());
                                cardItems.add(cardItem);
                            }
                        }

                        responseData.onResponse(cardItems);
                    }
                });
    }

    public static void bookmark(CardItem item, Context context) {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        if (item.getBookmarkBy().contains(Tools.getUniqueIdentification(context))) {
            item.getBookmarkBy().remove(Tools.getUniqueIdentification(context));
            db.collection("quotes").document(item.getId()).update("bookmarkBy", item.getBookmarkBy());
            return;
        }

        Map<String, Object> map = new HashMap<>();
        List<String> list = new ArrayList<>();
        list.add(Tools.getUniqueIdentification(context));
        map.put("bookmarkBy", list);
        db.collection("quotes").document(item.getId()).set(map, SetOptions.merge());
    }

    public static void like(CardItem item, MainActivity mainActivity) {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        if (item.getLikedBy().contains(Tools.getUniqueIdentification(mainActivity))) {
            item.getLikedBy().remove(Tools.getUniqueIdentification(mainActivity));
            db.collection("quotes").document(item.getId()).update("likedBy", item.getLikedBy());
            return;
        }

        Map<String, Object> map = new HashMap<>();
        List<String> list = new ArrayList<>();
        list.add(Tools.getUniqueIdentification(mainActivity));
        map.put("likedBy", list);
        db.collection("quotes").document(item.getId()).set(map, SetOptions.merge());
    }

    public static void getRandomQuote(final ResponseData responseData) {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        db.collection("quotes")
                .orderBy("id")
                .limit(1)
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        List<CardItem> cardItems = new ArrayList<>();
                        CardItem cardItem = task.getResult().getDocuments().get(0).toObject(CardItem.class);
                        cardItem.setId(task.getResult().getDocuments().get(0).getId());
                        cardItems.add(cardItem);
                        responseData.onResponse(cardItems);
                    }
                });
    }

    public interface ResponseData {
        void onResponse(List<CardItem> cardItems);
    }
}
