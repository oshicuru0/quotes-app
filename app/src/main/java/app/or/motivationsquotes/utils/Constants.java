package app.or.motivationsquotes.utils;

import android.content.Context;
import android.support.v4.content.ContextCompat;

import java.util.HashMap;

import app.or.motivationsquotes.R;

/**
 * Created by RESIDOVI on 9/20/2018.
 */

public class Constants {
    public static HashMap<String, Integer> getCategoryColors() {
        HashMap<String, Integer> categoryColors = new HashMap<>();
        categoryColors.put("Work", R.color.color1);
        categoryColors.put("Politics", R.color.color3);
        categoryColors.put("Leadership", R.color.color3);
        categoryColors.put("Inspirational", R.color.color5);
        categoryColors.put("Attitude", R.color.color7);
        categoryColors.put("Motivational", R.color.color9);
        categoryColors.put("Truth", R.color.color2);
        categoryColors.put("Humanity", R.color.color4);
        categoryColors.put("Love", R.color.color6);
        categoryColors.put("Peace", R.color.color6);
        categoryColors.put("Opportunity", R.color.color8);
        categoryColors.put("Enthusiasm", R.color.color10);
        categoryColors.put("Quote of the day", R.color.color11);
        categoryColors.put("Bookmarked", R.color.color12);
        return categoryColors;
    }

    public static Integer getColorForCategory(Context context, String category) {
        return ContextCompat.getColor(context, getCategoryColors().get(category));
    }

}
