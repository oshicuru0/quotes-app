package app.or.motivationsquotes.utils;

import android.content.Context;
import android.graphics.Rect;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import app.or.motivationsquotes.R;

/**
 * Created by RESIDOVI on 9/19/2018.
 */

public class MyDivider extends DividerItemDecoration {
    public MyDivider(Context context, int orientation) {
        super(context, orientation);
        super.setDrawable(context.getDrawable(R.drawable.divider));
    }

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
        super.getItemOffsets(outRect, view, parent, state);
        outRect.bottom = (int) Tools.convertDpToPixel(16, parent.getContext());
    }
}
