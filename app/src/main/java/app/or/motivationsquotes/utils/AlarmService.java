package app.or.motivationsquotes.utils;

import android.app.AlarmManager;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v4.app.JobIntentService;
import android.support.v4.app.NotificationCompat;

import java.util.List;

import app.or.motivationsquotes.R;
import app.or.motivationsquotes.activity.MainActivity_;
import app.or.motivationsquotes.category.singlecategory.CardItem;
import app.or.motivationsquotes.helper.FirebaseHelper;

public class AlarmService extends JobIntentService {
    public static final int JOB_ID = 100;

    public static void enqueueWork(Context context, Intent work) {
        enqueueWork(context, AlarmService.class, JOB_ID, work);
    }

    public static PendingIntent createPendingIntent(Context context, int flag) {
        Intent intent = new Intent(context, AlarmReceiver.class);
        return PendingIntent.getBroadcast(context, AlarmReceiver.REQUEST_CODE, intent, flag);
    }

    public static void cancelAlarm(Context context) {
        AlarmManager alarm = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        if (alarm == null) {
            return;
        }

        PendingIntent alarmPendingIntent = AlarmService.createPendingIntent(context, 0);
        alarm.cancel(alarmPendingIntent);
    }

    public static void startAlarm(Context context, long timestamp, long interval) {
        AlarmManager alarm = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        if (alarm == null) {
            return;
        }

        alarm.setRepeating(AlarmManager.RTC_WAKEUP, timestamp, interval, createPendingIntent(context, PendingIntent.FLAG_UPDATE_CURRENT));
    }

    @Override
    protected void onHandleWork(@NonNull Intent intent) {
        createNotification();
    }

    private void createNotification() {
        FirebaseHelper.getRandomQuote(new FirebaseHelper.ResponseData() {
            @Override
            public void onResponse(List<CardItem> cardItems) {
                CardItem item = cardItems.get(0);
                Intent intent = new Intent(AlarmService.this, MainActivity_.class);
                intent.putExtra("data", item);
                PendingIntent pendingIntent = PendingIntent.getActivity(AlarmService.this, 0, intent, PendingIntent.FLAG_ONE_SHOT);

                String CHANNEL_ID = "my_channel_01";
                NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                    CharSequence name = getString(R.string.common_google_play_services_notification_channel_name);
                    int importance = NotificationManager.IMPORTANCE_HIGH;
                    NotificationChannel mChannel = new NotificationChannel(CHANNEL_ID, name, importance);
                    if (notificationManager != null) {
                        notificationManager.createNotificationChannel(mChannel);
                    }
                }

                NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(AlarmService.this, CHANNEL_ID)
                        .setSmallIcon(R.mipmap.ic_launcher)
                        .setAutoCancel(true)
                        .setContentTitle(item.getAuthor())
                        .setContentText(item.getQuote())
                        .setContentIntent(pendingIntent);

                if (notificationManager != null) {
                    notificationManager.notify(0, notificationBuilder.build());
                }
            }
        });
    }
}