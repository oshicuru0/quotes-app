package app.or.motivationsquotes.utils;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by RESIDOVI on 6/27/2018.
 */

@SuppressLint("ApplySharedPref")
public class LocalStorage {

    public static void setAlarm(Context context, boolean shouldSet, String time) {
        SharedPreferences sharedPreferences = context.getSharedPreferences("LOCAL_STORAGE", Context.MODE_PRIVATE);
        sharedPreferences.edit().putBoolean("alarm_set", shouldSet).putString("alarm_time", time).commit();
    }

    public static Object[] getAlarm(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences("LOCAL_STORAGE", Context.MODE_PRIVATE);
        return new Object[]{sharedPreferences.getBoolean("alarm_set", false), sharedPreferences.getString("alarm_time", null)};
    }
}
