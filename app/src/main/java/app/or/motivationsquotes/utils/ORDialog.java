package app.or.motivationsquotes.utils;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.AppCompatEditText;
import android.view.View;

import app.or.motivationsquotes.R;

public class ORDialog extends BaseDialog<ORDialog> implements View.OnClickListener {
    private Context context;
    private int view;
    private View inflate;
    private float scale = 0.95f;
    private ButtonClick listener;

    public ORDialog(Context context, int view) {
        super(context);
        this.context = context;
        this.view = view;
        inflate = View.inflate(context, view, null);

    }

    public ORDialog(Context context, int view, int scale) {
        super(context);
        this.context = context;
        this.view = view;
        inflate = View.inflate(context, view, null);
        this.scale = scale;
    }

    private ORDialog(Context context) {
        super(context);
        this.context = context;
        inflate = View.inflate(context, R.layout.dialog_layout, null);
        final AppCompatEditText title = inflate.findViewById(R.id.authorName);
        inflate.findViewById(R.id.save).setOnClickListener(view -> {
            if (title.getText().toString().isEmpty()) {
                title.setError("Please enter author name, or surname");
                return;
            }
            dismiss();
            if (listener == null) {
                return;
            }
            listener.onYes(title.getText().toString());
        });
    }

    public static ORDialog createDialog(Activity activity) {
        return new ORDialog(activity);
    }

    @Override
    public View onCreateView() {
        widthScale(scale);
        return inflate;
    }

    @Override
    public void setUiBeforShow() {
    }

    public View getView() {
        return inflate;
    }

    @Override
    public void onClick(View v) {

    }

    public ORDialog setListener(ButtonClick listener) {
        this.listener = listener;
        return this;
    }

    public interface ButtonClick {
        void onYes(String text);

        void onCancel();
    }


}