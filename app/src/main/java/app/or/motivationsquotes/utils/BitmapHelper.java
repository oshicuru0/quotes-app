package app.or.motivationsquotes.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import app.or.motivationsquotes.R;
import app.or.motivationsquotes.activity.MainActivity;
import app.or.motivationsquotes.category.singlecategory.CardItem;

/**
 * Created by RESIDOVI on 4/27/2018.
 */

public class BitmapHelper {
    public static void createBitmap(final CardItem cardItem, final MainActivity mainActivity, final DownloadQuote downloadQuote) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                Bitmap bitmap = createBitmapFromLayoutWithText(mainActivity, cardItem);
                String savedImage = MediaStore.Images.Media.insertImage(mainActivity.getContentResolver(), bitmap, "Image", "Desc");
                downloadQuote.onCompleted(null);
            }
        }).start();
    }

    private static Bitmap createBitmapFromLayoutWithText(MainActivity mainActivity, final CardItem item) {
        LayoutInflater mInflater = (LayoutInflater) mainActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        RelativeLayout view = new RelativeLayout(mainActivity);
        mInflater.inflate(R.layout.card_item_download, view, true);

        TextView author = view.findViewById(R.id.author);
        TextView quote = view.findViewById(R.id.quote);
        final ImageView authorImage = view.findViewById(R.id.authorImage);


        author.setText(String.format("- %s", item.getAuthor()));
        quote.setText(item.getQuote());

        mainActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Picasso.get().load(item.getAuthorImage()).into(authorImage);
            }
        });

        view.setLayoutParams(new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT));
        view.measure(View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED), View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED));
        view.layout(0, 0, view.getMeasuredWidth(), view.getMeasuredHeight());
        Bitmap bitmap = Bitmap.createBitmap(view.getMeasuredWidth(), view.getMeasuredHeight(), Bitmap.Config.ARGB_8888);
        Canvas c = new Canvas(bitmap);
        view.draw(c);
        return bitmap;
    }

    public interface DownloadQuote {
        void onCompleted(String uri);
    }
}
