package app.or.motivationsquotes.utils;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager.LayoutParams;
import android.view.animation.Animation;
import android.view.animation.Interpolator;
import android.view.animation.TranslateAnimation;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

public abstract class BaseDialog<T extends BaseDialog<T>> extends Dialog {
    protected String mTag;
    protected Context mContext;
    protected DisplayMetrics mDisplayMetrics;
    protected boolean mCancel;
    protected float mWidthScale = 1;
    protected float mHeightScale;

    private BaseAnimatorSet mShowAnim;
    private BaseAnimatorSet mDismissAnim;
    protected LinearLayout mLlTop;
    protected LinearLayout mLlControlHeight;
    protected View mOnCreateView;
    private boolean mIsShowAnim;
    private boolean mIsDismissAnim;
    protected float mMaxHeight;
    private boolean mIsPopupStyle;
    private boolean mAutoDismiss;
    private long mAutoDismissDelay = 1500;
    private Handler mHandler = new Handler(Looper.getMainLooper());

    public BaseDialog(Context context) {
        super(context);
        setDialogTheme();
        mContext = context;
        mTag = getClass().getSimpleName();
        setCanceledOnTouchOutside(true);
        Log.d(mTag, "constructor");
    }

    public BaseDialog(Context context, boolean isPopupStyle) {
        this(context);
        mIsPopupStyle = isPopupStyle;
    }

    private void setDialogTheme() {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        getWindow().addFlags(LayoutParams.FLAG_DIM_BEHIND);
    }

    public abstract View onCreateView();

    public void onViewCreated(View inflate) {
    }

    public abstract void setUiBeforShow();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d(mTag, "onCreate");
        mDisplayMetrics = mContext.getResources().getDisplayMetrics();
        mMaxHeight = mDisplayMetrics.heightPixels - StatusBarUtils.getHeight(mContext);

        mLlTop = new LinearLayout(mContext);
        mLlTop.setGravity(Gravity.CENTER);

        mLlControlHeight = new LinearLayout(mContext);
        mLlControlHeight.setOrientation(LinearLayout.VERTICAL);

        mOnCreateView = onCreateView();
        mLlControlHeight.addView(mOnCreateView);
        mLlTop.addView(mLlControlHeight);
        onViewCreated(mOnCreateView);

        if (mIsPopupStyle) {
            setContentView(mLlTop, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT));
        } else {
            setContentView(mLlTop, new ViewGroup.LayoutParams(mDisplayMetrics.widthPixels, (int) mMaxHeight));
        }

        mLlTop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mCancel) {
                    dismiss();
                }
            }
        });

        mOnCreateView.setClickable(true);
    }

    public View getCreateView() {
        return mOnCreateView;
    }

    @Override
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        Log.d(mTag, "onAttachedToWindow");

        setUiBeforShow();

        int width;
        if (mWidthScale == 0) {
            width = ViewGroup.LayoutParams.WRAP_CONTENT;
        } else {
            width = (int) (mDisplayMetrics.widthPixels * mWidthScale);
        }

        int height;
        if (mHeightScale == 0) {
            height = ViewGroup.LayoutParams.WRAP_CONTENT;
        } else if (mHeightScale == 1) {
            height = (int) mMaxHeight;
        } else {
            height = (int) (mMaxHeight * mHeightScale);
        }

        mLlControlHeight.setLayoutParams(new LinearLayout.LayoutParams(width, height));

        if (mShowAnim != null) {
            mShowAnim.listener(new BaseAnimatorSet.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animator) {
                    mIsShowAnim = true;
                }

                @Override
                public void onAnimationRepeat(Animator animator) {
                }

                @Override
                public void onAnimationEnd(Animator animator) {
                    mIsShowAnim = false;
                    delayDismiss();
                }

                @Override
                public void onAnimationCancel(Animator animator) {
                    mIsShowAnim = false;
                }
            }).playOn(mLlControlHeight);
        } else {
            BaseAnimatorSet.reset(mLlControlHeight);
            delayDismiss();
        }
    }


    @Override
    public void setCanceledOnTouchOutside(boolean cancel) {
        this.mCancel = cancel;
        super.setCanceledOnTouchOutside(cancel);
    }

    @Override
    public void show() {
        Log.d(mTag, "show");
        super.show();
    }


    @Override
    protected void onStart() {
        super.onStart();
        Log.d(mTag, "onStart");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d(mTag, "onStop");
    }

    @Override
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        Log.d(mTag, "onDetachedFromWindow");
    }

    @Override
    public void dismiss() {
        Log.d(mTag, "dismiss");
        if (mDismissAnim != null) {
            mDismissAnim.listener(new BaseAnimatorSet.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animator) {
                    mIsDismissAnim = true;
                }

                @Override
                public void onAnimationRepeat(Animator animator) {
                }

                @Override
                public void onAnimationEnd(Animator animator) {
                    mIsDismissAnim = false;
                    superDismiss();
                }

                @Override
                public void onAnimationCancel(Animator animator) {
                    mIsDismissAnim = false;
                    superDismiss();
                }
            }).playOn(mLlControlHeight);
        } else {
            superDismiss();
        }
    }

    public void superDismiss() {
        super.dismiss();
    }

    public void show(int animStyle) {
        Window window = getWindow();
        window.setWindowAnimations(animStyle);
        show();
    }

    public void showAtLocation(int gravity, int x, int y) {
        if (mIsPopupStyle) {
            Window window = getWindow();
            LayoutParams params = window.getAttributes();
            window.setGravity(gravity);
            params.x = x;
            params.y = y;
        }

        show();
    }

    public void showAtLocation(int x, int y) {
        int gravity = Gravity.LEFT | Gravity.TOP;
        showAtLocation(gravity, x, y);
    }

    public T dimEnabled(boolean isDimEnabled) {
        if (isDimEnabled) {
            getWindow().addFlags(LayoutParams.FLAG_DIM_BEHIND);
        } else {
            getWindow().clearFlags(LayoutParams.FLAG_DIM_BEHIND);
        }
        return (T) this;
    }

    public T widthScale(float widthScale) {
        this.mWidthScale = widthScale;
        return (T) this;
    }

    public T heightScale(float heightScale) {
        mHeightScale = heightScale;
        return (T) this;
    }

    public T showAnim(BaseAnimatorSet showAnim) {
        mShowAnim = showAnim;
        return (T) this;
    }

    public T dismissAnim(BaseAnimatorSet dismissAnim) {
        mDismissAnim = dismissAnim;
        return (T) this;
    }

    public T autoDismiss(boolean autoDismiss) {
        mAutoDismiss = autoDismiss;
        return (T) this;
    }

    public T autoDismissDelay(long autoDismissDelay) {
        mAutoDismissDelay = autoDismissDelay;
        return (T) this;
    }

    private void delayDismiss() {
        if (mAutoDismiss && mAutoDismissDelay > 0) {
            mHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    dismiss();
                }
            }, mAutoDismissDelay);
        }
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        if (mIsDismissAnim || mIsShowAnim || mAutoDismiss) {
            return true;
        }
        return super.dispatchTouchEvent(ev);
    }

    @Override
    public void onBackPressed() {
        if (mIsDismissAnim || mIsShowAnim || mAutoDismiss) {
            return;
        }
        super.onBackPressed();
    }

    /** dp to px */
    protected int dp2px(float dp) {
        final float scale = mContext.getResources().getDisplayMetrics().density;
        return (int) (dp * scale + 0.5f);
    }

    public static class StatusBarUtils {
        public static int getHeight(Context context) {
            int statusBarHeight = 0;
            int resourceId = context.getResources().getIdentifier("status_bar_height", "dimen", "android");
            if (resourceId > 0) {
                statusBarHeight = context.getResources().getDimensionPixelSize(resourceId);
            }
            return statusBarHeight;
        }

    }

    public abstract static class BaseAnimatorSet {
        protected long duration = 500;
        protected AnimatorSet animatorSet = new AnimatorSet();
        private Interpolator interpolator;
        private long delay;
        private AnimatorListener listener;

        public abstract void setAnimation(View view);

        protected void start(final View view) {
            reset(view);
            setAnimation(view);

            animatorSet.setDuration(duration);
            if (interpolator != null) {
                animatorSet.setInterpolator(interpolator);
            }

            if (delay > 0) {
                animatorSet.setStartDelay(delay);
            }

            if (listener != null) {
                animatorSet.addListener(new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationStart(Animator animator) {
                        listener.onAnimationStart(animator);
                    }

                    @Override
                    public void onAnimationRepeat(Animator animator) {
                        listener.onAnimationRepeat(animator);
                    }

                    @Override
                    public void onAnimationEnd(Animator animator) {
                        listener.onAnimationEnd(animator);
                    }

                    @Override
                    public void onAnimationCancel(Animator animator) {
                        listener.onAnimationCancel(animator);
                    }
                });
            }

            animatorSet.start();
        }

        public static void reset(View view) {
            view.setAlpha(1);
            view.setScaleX(1);
            view.setScaleY(1);
            view.setTranslationX(0);
            view.setTranslationY(0);
            view.setRotation(0);
            view.setRotationY(0);
            view.setRotationX(0);
        }

        public BaseAnimatorSet duration(long duration) {
            this.duration = duration;
            return this;
        }

        public BaseAnimatorSet delay(long delay) {
            this.delay = delay;
            return this;
        }

        public BaseAnimatorSet interpolator(Interpolator interpolator) {
            this.interpolator = interpolator;
            return this;
        }

        public BaseAnimatorSet listener(AnimatorListener listener) {
            this.listener = listener;
            return this;
        }

        public void playOn(View view) {
            start(view);
        }

        public interface AnimatorListener {
            void onAnimationStart(Animator animator);

            void onAnimationRepeat(Animator animator);

            void onAnimationEnd(Animator animator);

            void onAnimationCancel(Animator animator);
        }
    }

    public abstract static class BottomBaseDialog<T extends BottomBaseDialog<T>> extends BottomTopBaseDialog<T> {
        public BottomBaseDialog(Context context, View animateView) {
            super(context);
            mAnimateView = animateView;

            mInnerShowAnim = new TranslateAnimation(Animation.RELATIVE_TO_SELF, 0, Animation.RELATIVE_TO_SELF, 0,
                    Animation.RELATIVE_TO_SELF, 1f, Animation.RELATIVE_TO_SELF, 0);

            mInnerDismissAnim = new TranslateAnimation(Animation.RELATIVE_TO_SELF, 0, Animation.RELATIVE_TO_SELF, 0,
                    Animation.RELATIVE_TO_SELF, 0, Animation.RELATIVE_TO_SELF, 1);
        }

        public BottomBaseDialog(Context context) {
            this(context, null);
        }

        @Override
        protected void onStart() {
            super.onStart();
            mLlTop.setLayoutParams(new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT,
                    FrameLayout.LayoutParams.MATCH_PARENT));
            mLlTop.setGravity(Gravity.BOTTOM);
            getWindow().setGravity(Gravity.BOTTOM);
            mLlTop.setPadding(mLeft, mTop, mRight, mBottom);
        }

        @Override
        public void onAttachedToWindow() {
            super.onAttachedToWindow();
            showWithAnim();
        }

        @Override
        public void dismiss() {
            dismissWithAnim();
        }

        private BaseAnimatorSet mWindowInAs;
        private BaseAnimatorSet mWindowOutAs;

        @Override
        protected BaseAnimatorSet getWindowInAs() {
            if (mWindowInAs == null) {
                mWindowInAs = new WindowInAs();
            }
            return mWindowInAs;
        }

        @Override
        protected BaseAnimatorSet getWindowOutAs() {
            if (mWindowOutAs == null) {
                mWindowOutAs = new WindowOutAs();
            }
            return mWindowOutAs;
        }


        private class WindowInAs extends BaseAnimatorSet {
            @Override
            public void setAnimation(View view) {
                ObjectAnimator oa1 = ObjectAnimator.ofFloat(view, "scaleX", 1f, 0.9f);
                ObjectAnimator oa2 = ObjectAnimator.ofFloat(view, "scaleY", 1f, 0.9f);
                animatorSet.playTogether(oa1, oa2);
            }
        }

        private class WindowOutAs extends BaseAnimatorSet {
            @Override
            public void setAnimation(View view) {
                ObjectAnimator oa1 = ObjectAnimator.ofFloat(view, "scaleX", 0.9f, 1f);
                ObjectAnimator oa2 = ObjectAnimator.ofFloat(view, "scaleY", 0.9f, 1f);
                animatorSet.playTogether(oa1, oa2);
            }
        }
    }

    public abstract static class BottomTopBaseDialog<T extends BottomTopBaseDialog<T>> extends BaseDialog<T> {
        protected View mAnimateView;
        private BaseAnimatorSet mWindowInAs;
        private BaseAnimatorSet mWindowOutAs;
        protected Animation mInnerShowAnim;
        protected Animation mInnerDismissAnim;
        protected long mInnerAnimDuration = 150;
        protected boolean mIsInnerShowAnim;
        protected boolean mIsInnerDismissAnim;
        protected int mLeft, mTop, mRight, mBottom;

        public BottomTopBaseDialog(Context context) {
            super(context);
        }

        public T innerAnimDuration(long innerAnimDuration) {
            mInnerAnimDuration = innerAnimDuration;
            return (T) this;
        }

        public T padding(int left, int top, int right, int bottom) {
            mLeft = left;
            mTop = top;
            mRight = right;
            mBottom = bottom;
            return (T) this;
        }

        protected void showWithAnim() {
            if (mInnerShowAnim != null) {
                mInnerShowAnim.setDuration(mInnerAnimDuration);
                mInnerShowAnim.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {
                        mIsInnerShowAnim = true;
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                        mIsInnerShowAnim = false;
                    }
                });
                mLlControlHeight.startAnimation(mInnerShowAnim);
            }

            if (mAnimateView != null) {
                if (getWindowInAs() != null) {
                    mWindowInAs = getWindowInAs();
                }
                mWindowInAs.duration(mInnerAnimDuration).playOn(mAnimateView);
            }
        }

        protected void dismissWithAnim() {
            if (mInnerDismissAnim != null) {
                mInnerDismissAnim.setDuration(mInnerAnimDuration);
                mInnerDismissAnim.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {
                        mIsInnerDismissAnim = true;
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                        mIsInnerDismissAnim = false;
                        superDismiss();
                    }
                });

                mLlControlHeight.startAnimation(mInnerDismissAnim);
            } else {
                superDismiss();
            }

            if (mAnimateView != null) {
                if (getWindowOutAs() != null) {
                    mWindowOutAs = getWindowOutAs();
                }
                mWindowOutAs.duration(mInnerAnimDuration).playOn(mAnimateView);
            }
        }

        @Override
        public boolean dispatchTouchEvent(MotionEvent ev) {
            if (mIsInnerDismissAnim || mIsInnerShowAnim) {
                return true;
            }
            return super.dispatchTouchEvent(ev);
        }

        @Override
        public void onBackPressed() {
            if (mIsInnerDismissAnim || mIsInnerShowAnim) {
                return;
            }
            super.onBackPressed();
        }

        protected abstract BaseAnimatorSet getWindowInAs();

        protected abstract BaseAnimatorSet getWindowOutAs();
    }
}