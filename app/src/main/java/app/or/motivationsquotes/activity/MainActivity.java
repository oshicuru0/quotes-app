package app.or.motivationsquotes.activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import app.or.motivationsquotes.R;
import app.or.motivationsquotes.category.CategoryItemsFragment_;
import app.or.motivationsquotes.category.singlecategory.CardItem;
import app.or.motivationsquotes.category.singlecategory.CategoryFragment_;
import app.or.motivationsquotes.utils.BitmapHelper;
import app.or.motivationsquotes.utils.ORDialog;

import static app.or.motivationsquotes.utils.Constants.getColorForCategory;

@SuppressLint("Registered")
@EActivity(R.layout.activity_main)
public class MainActivity extends AppCompatActivity {

    @ViewById
    public TextView title;

    @ViewById
    public View toolbar;

    @ViewById
    public View overlay;

    @ViewById
    public ImageButton hamburger;

    public String currentFragmentTitle = CategoryFragment_.class.getSimpleName();

    @AfterViews
    public void onCreate() {
        Intent intent = getIntent();
        if (intent != null) {
            Bundle bundle = intent.getExtras();
            if (bundle != null) {
                CardItem item = (CardItem) bundle.get("data");
                if (item != null) {
                    changeFragment(new CategoryItemsFragment_.FragmentBuilder_().category(item.getId()).build());
                    return;
                }
            }
        }

        changeFragment(new CategoryFragment_.FragmentBuilder_().build());
    }

    public void changeFragment(Fragment fragment) {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.container, fragment)
                .commitAllowingStateLoss();
        currentFragmentTitle = fragment.getClass().getSimpleName();
    }

    public void openCategory(String category) {
        changeFragment(new CategoryItemsFragment_.FragmentBuilder_().category(category).build());
        toolbar.setBackgroundColor(getColorForCategory(this, category));
        overlay.setBackgroundColor(getColorForCategory(this, category));
        getWindow().setStatusBarColor(getColorForCategory(this, category));
    }

    public void searchByAuthor(String author) {
        changeFragment(new CategoryItemsFragment_.FragmentBuilder_().category("authorName,"+author).build());
        toolbar.setBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimaryDark));
        getWindow().setStatusBarColor(ContextCompat.getColor(MainActivity.this, R.color.colorPrimaryDark));
        overlay.setBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimaryDark));
    }


    @Click(R.id.hamburger)
    public void onHamburger() {
        changeFragment(new CategoryFragment_.FragmentBuilder_().build());
        toolbar.setBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimaryDark));
        getWindow().setStatusBarColor(ContextCompat.getColor(MainActivity.this, R.color.colorPrimaryDark));
        overlay.setBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimaryDark));
    }

    @Click(R.id.settings)
    public void onSettings() {
        startActivity(new Intent(MainActivity.this, SettingsActivity_.class));
    }

    @Click(R.id.search)
    public void onSearch() {
        ORDialog.createDialog(this).setListener(new ORDialog.ButtonClick() {
            @Override
            public void onYes(String text) {
                searchByAuthor(text);
            }

            @Override
            public void onCancel() {

            }
        }).show();
    }

    public void setTitle(String title) {
        this.title.setText(title);
    }

    boolean doubleBackToExitPressedOnce = false;

    @Override
    public void onBackPressed() {
        if (currentFragmentTitle.equals(CategoryFragment_.class.getSimpleName())) {
            if (doubleBackToExitPressedOnce) {
                super.onBackPressed();
                return;
            }
        } else {
            onHamburger();
            return;
        }


        this.doubleBackToExitPressedOnce = true;
        Snackbar.make(overlay, "Please click BACK again to exit", Snackbar.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 2000);
    }

    private CardItem itemForDownload = null;

    public void download(CardItem item) {
        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE) ==
                PackageManager.PERMISSION_GRANTED) {
            BitmapHelper.createBitmap(item, MainActivity.this, new BitmapHelper.DownloadQuote() {
                @Override
                public void onCompleted(String uri) {
                    Snackbar.make(overlay, "Quote downloaded to gallery.", Snackbar.LENGTH_SHORT).show();
                }
            });
        } else {
            itemForDownload = item;

            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    1001);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 1001 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            BitmapHelper.createBitmap(itemForDownload, MainActivity.this, new BitmapHelper.DownloadQuote() {
                @Override
                public void onCompleted(String uri) {
                    itemForDownload = null;
                    Snackbar.make(overlay, "Quote downloaded to gallery.", Snackbar.LENGTH_SHORT).show();
                }
            });
        }
    }
}
