package app.or.motivationsquotes.activity;

import android.annotation.SuppressLint;
import android.app.TimePickerDialog;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SwitchCompat;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.TimePicker;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import app.or.motivationsquotes.R;
import app.or.motivationsquotes.utils.AlarmService;
import app.or.motivationsquotes.utils.LocalStorage;

@SuppressLint("Registered")
@EActivity(R.layout.activity_settings)
public class SettingsActivity extends AppCompatActivity {

    @ViewById
    public SwitchCompat reminder;

    @ViewById
    public TextView time;

    private SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm", Locale.CANADA);

    private long timestamp = System.currentTimeMillis();

    @AfterViews
    public void onCreate() {
        Object[] alarmData = LocalStorage.getAlarm(SettingsActivity.this);
        boolean shouldActivateAlarm = (boolean) alarmData[0];
        if (shouldActivateAlarm) {
            reminder.setChecked(true);
            time.setText(String.valueOf(alarmData[1]));
        } else {
            reminder.setChecked(false);
            time.setText(dateFormat.format(Calendar.getInstance().getTime()));
        }

        reminder.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    AlarmService.startAlarm(SettingsActivity.this, timestamp, 86400000);
                    LocalStorage.setAlarm(SettingsActivity.this, true, time.getText().toString());
                } else {
                    AlarmService.cancelAlarm(SettingsActivity.this);
                    LocalStorage.setAlarm(SettingsActivity.this, false, null);
                }
            }
        });
    }

    @Click(resName = "timeBox")
    protected void onTime() {
        final Calendar calendar = Calendar.getInstance();
        TimePickerDialog timePickerDialog = new TimePickerDialog(SettingsActivity.this, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int i, int i1) {
                calendar.set(Calendar.HOUR_OF_DAY, i);
                calendar.set(Calendar.MINUTE, i1);
                time.setText(dateFormat.format(calendar.getTime()));
                timestamp = calendar.getTimeInMillis();
                AlarmService.cancelAlarm(SettingsActivity.this);
                AlarmService.startAlarm(SettingsActivity.this, timestamp, 86400000);
                LocalStorage.setAlarm(SettingsActivity.this, true, time.getText().toString());
                reminder.setChecked(true);
            }
        }, calendar.get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.MINUTE), true);
        timePickerDialog.show();
    }

    @Click(R.id.hamburger)
    public void onHamburger() {
        onBackPressed();
    }

    @Click(R.id.fb)
    public void onFb() {
        try {
            getPackageManager().getPackageInfo("com.facebook.katana", 0);
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("fb://page/145004876202065")));
        } catch (Exception e) {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.facebook.com/findtruemotivation/")));
        }
    }

    @Click(R.id.ig)
    public void onIg() {
        Uri uri = Uri.parse("https://www.instagram.com/_motive4you/?hl=en");
        Intent likeIng = new Intent(Intent.ACTION_VIEW, uri);

        likeIng.setPackage("com.instagram.android");

        try {
            startActivity(likeIng);
        } catch (ActivityNotFoundException e) {
            startActivity(new Intent(Intent.ACTION_VIEW,
                    Uri.parse("https://www.instagram.com/_motive4you/?hl=en")));
        }
    }

    @Click(R.id.twitter)
    public void onTw() {
        Uri uri = Uri.parse("https://twitter.com/motive4you_");
        Intent likeIng = new Intent(Intent.ACTION_VIEW, uri);

        likeIng.setPackage("com.twitter.android");

        try {
            startActivity(likeIng);
        } catch (ActivityNotFoundException e) {
            startActivity(new Intent(Intent.ACTION_VIEW,
                    Uri.parse("https://twitter.com/motive4you_")));
        }
    }
}
